
package comp2931.cwk1;

import static org.junit.Assert.*;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;
import java.util.Calendar;


public class DateTest {
  private Calendar mockCalendar;

  /* @Before
  public void setUpDateMock(){
    mockCalendar = mock(Calendar.class);
    when(Calendar.getInstance()).thenReturn();
    when(Calendar.get(Calendar.YEAR)).thenReturn(2017);
    when(Calendar.get(Calendar.MONTH)).thenReturn(10);
    when(Calendar.get(Calendar.DAY_OF_MONTH)).thenReturn(9);
  } */

  @Test
  public void todayDateBasic() {
    Calendar now = Calendar.getInstance();
    Date nowDate = new Date();

    assertThat(nowDate.getYear(), is(now.get(Calendar.YEAR)));
    assertThat(nowDate.getMonth(), is(now.get(Calendar.MONTH) + 1));
    assertThat(nowDate.getDay(), is(now.get(Calendar.DAY_OF_MONTH)));

  }

  @Test
  public void validDateConstructor() {
    new Date(1920, 4, 30);
    new Date(1988, 2, 29);
  }

  @Test(expected=IllegalArgumentException.class)
  public void dateZero() {
    new Date(0, 0, 0);
  }

  @Test(expected=IllegalArgumentException.class)
  public void dateTooHigh() {
    new Date(2007, 13, 40);
  }

  @Test(expected=IllegalArgumentException.class)
  public void invalidLeapYear() {
    new Date(2017, 2, 29);
  }

  @Test(expected=IllegalArgumentException.class)
  public void mismatchedDayMonth() {
    new Date(2003, 9, 31);
  }


  @Test
  public void dateToString() {
    Date yearOne = new Date(1, 1, 1);
    Date birthday = new Date(1998, 4, 24);

    assertThat(yearOne.toString(), is("0001-01-01"));
    assertThat(birthday.toString(), is("1998-04-24"));
  }


  @Test
  public void dateEquals() {
    Date compare = new Date(1998, 8, 5);

    assertTrue(compare.equals(compare));
    assertTrue(compare.equals(new Date(1998, 8, 5)));
    assertFalse(compare.equals(new Date(1997, 8, 5)));
    assertFalse(compare.equals(new Date(1998, 9, 5)));
    assertFalse(compare.equals(new Date(1998, 8, 6)));
  }


  @Test
  public void getDayOfYearTest() {
    Date januaryFirst = new Date(2014, 1, 1);
    Date newYearsEve = new Date(2014, 12, 31);
    Date leapNewYearsEve = new Date(2000, 12, 31);
    Date leapMiddleOfYear = new Date(1996, 5, 8);

    assertThat(januaryFirst.getDayOfYear(), is(1));
    assertThat(newYearsEve.getDayOfYear(), is(365));
    assertThat(leapNewYearsEve.getDayOfYear(), is(366));
    assertThat(leapMiddleOfYear.getDayOfYear(), is(129));
  }

}
