// Class for COMP2931 Coursework 1

package comp2931.cwk1;

import java.util.Calendar;
import java.lang.IllegalArgumentException;

/**
 * Simple representation of a date.
 */
public class Date {

  private int year;
  private int month;
  private int day;

  /**
   * Creates a date using the current year, month and day.
   *
   */
  public Date() {
    Calendar timeNow = Calendar.getInstance();
    year = timeNow.get(Calendar.YEAR);
    month = (timeNow.get(Calendar.MONTH)) + 1;
    day = timeNow.get(Calendar.DAY_OF_MONTH);
  }


  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) throws IllegalArgumentException {

    if (y <= 0 || m <= 0 || d <= 0 || m > 12 || d > 31) {
      throw new IllegalArgumentException("Invalid date value.");
    }

    else if (m == 2) {

      if ((y % 4) == 0 && d > 29) {
  	    throw new IllegalArgumentException("Invalid date value.");
      }

      else if (((y % 4) != 0 || (y % 100) == 0) && d > 28) {
        throw new IllegalArgumentException("Invalid date value.");
      }
    }

    else if (m == 4 || m == 6 || m == 9 || m == 11) {
      if (d > 30) {
        throw new IllegalArgumentException("Invalid date value.");
      }
    }

    year = y;
    month = m;
    day = d;
  }

  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }


  /**
   * Obtains the number of the day in the given year (i.e January 1st = 1).
   *
   * @return The day in the given year
   */
  public int getDayOfYear() {
    int dayOfYear = 0;

    // for loop to work out the total of the previous months
    for (int i = 1; i < month; i++) {
      if (i == 2) {
        if (year % 4 == 0) {
          dayOfYear += 29;
        }

        else {
          dayOfYear += 28;
        }
      }

      else if (i == 4 || i == 6 || i == 9 || i == 11) {
        dayOfYear += 30;
      }

      else {
        dayOfYear += 31;
      }
    }

    dayOfYear += day;

    return dayOfYear;
  }



  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%02d", year, month, day);
  }



  /**
   * Checks if two Date objects are equal.
   *
   * @param other The other Date object, to be compared to the current one.
   *
   * @return True or false value
   */
  public boolean equals(Date other) {
    if (year == other.getYear() && month == other.getMonth() && day == other.getDay()) {
      return true;
    }
    return false;
  }

}
